(function($, window, document) {

  function bannerform() {
    $('#getInTouchForm').validate({
      invalidHandler: function(event, validator) {
        var errors = validator.numberOfInvalids();
        if (errors) {
          $("#submit").prop('disabled', false);
          document.getElementById("submit").value="SUBMIT";
        }
      },
      rules: {
        field: {
          email: true,
          required: true,
        },
        tel: {
          required: true,
          phoneUK: true,
          minlength: 10,
          maxlength: 14
        },
        sn: {
          required: true,
          nameUK:true,
          rangelength: [2, 50]
        },
        fn: {
          required: true,
          nameUK:true,
          rangelength: [2, 50]
        },
        em: {
          required: true,

        },
      },

      messages: {
        Tel: {
          min: jQuery.validator.format("Please enter a valid number"),
          max: jQuery.validator.format("Please enter a valid number")
        }
      }
    });

    $('#getInTouchForm').on('submit', function(){
      if($('#getInTouchForm').valid()) {
        $("#submit").attr('disabled', 'disabled');
        document.getElementById("submit").value="Please Wait...";
      }
    });

    $.validator.addMethod('phoneUK', function(phone_number, element) {
      return this.optional(element) || phone_number.length > 9 &&
      phone_number.match(/^(\(?(0|\+44)[1-9]{1}\d{1,4}?\)?\s?\d{3,4}\s?\d{3,4})$/);

    }, 'Please enter a valid number'
  );

  $.validator.addMethod('nameUK', function(phone_number, element) {
    return this.optional(element) || phone_number.length > 1 &&
    phone_number.match(/^([ \u00c0-\u01ffa-zA-Z'\-])+$/);
  }, 'Please enter a valid Name'
);

$.extend(jQuery.validator.messages, {
  email: "Please enter a valid email."
});
}

$(function() {
  bannerform();
});
}(window.jQuery, window, document));
