(function($, window, document) {

  function anchorscroll() {
    $('a[href*=\\#]').on('click', function(event){
        event.preventDefault();

        $('html, body').animate({
            scrollTop: $( $.attr(this, 'href') ).offset().top
        }, 1000);
    });
  }

  function modal() {
    var iframe = document.querySelector('iframe');
    var player = new Vimeo.Player(iframe);

    $('.modal').on('click', function(e) {
      $('.videomodal').fadeIn();
      $('body').addClass('open');
      player.play();

      e.preventDefault();
    });

    $('.modal-close').on('click', function() {
      $('.videomodal').fadeOut();
      $('body').removeClass('open');
      player.pause();
    });
  }

 $(function() {
  anchorscroll();
  modal();
 });
}(window.jQuery, window, document));
